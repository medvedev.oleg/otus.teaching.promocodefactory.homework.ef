using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Services.Contracts;

namespace Services.Abstractions
{
    /// <summary>
    /// Интерфейс сервиса работы с предпочтениями.
    /// </summary>
    public interface IPreferencesService
    {
        /// <summary>
        /// Получить все предпочтения
        /// </summary> 
        Task<List<PreferenceDto>> GetAllAsync();         
    }
}