using Services.Contracts;

namespace Services.Abstractions
{
    /// <summary>
    /// Интерфейс сервиса работы с ролями.
    /// </summary>
    public interface IRoleService
    { 
        /// <summary>
        /// Получить все роли.
        /// </summary> 
        Task<List<RoleDto>> GetAllAsync();
    }
}