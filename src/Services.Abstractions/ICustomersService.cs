using Services.Contracts;

namespace Services.Abstractions
{
    /// <summary>
    /// Интерфейс сервиса работы с персоналом.
    /// </summary>
    public interface ICustomersService
    {
        /// <summary>
        /// Получить пользователя.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <returns> Dto</returns>
        Task<CustomerDto> GetAsync(Guid id);

        /// <summary>
        /// Получить всех пользователей.
        /// </summary> 
        Task<List<CustomerDto>> GetAllAsync();


        /// <summary>
        /// Создать пользователя.
        /// </summary>
        /// <param name="CreatingCustomerDto"> ДТО </param>
        Task<Guid> CreateAsync(CreatingCustomerDto creatingCustomerDto);

        /// <summary>
        /// Изменить пользователя.
        /// </summary>
        /// <param name="id"> Иентификатор. </param>
        /// <param name="UpdatingCustomerDto"> ДТО </param>
        Task UpdateAsync(Guid id, UpdatingCustomerDto updatingCustomerDto);

        /// <summary>
        /// Удалить пользователя.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        Task DeleteAsync(Guid id);
    }
}