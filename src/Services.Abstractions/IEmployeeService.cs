using Services.Contracts;

namespace Services.Abstractions
{
    /// <summary>
    /// Интерфейс сервиса работы с персоналом.
    /// </summary>
    public interface IEmployeeService
    {
        /// <summary>
        /// Получить сотрудника.
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <returns> Dto сотрудника. </returns>
        Task<EmployeeDto> GetAsync(Guid id);

        /// <summary>
        /// Получить всех сотрудников.
        /// </summary> 
        Task<List<EmployeeDto>> GetAllAsync();


        ///// <summary>
        ///// Создать курс.
        ///// </summary>
        ///// <param name="creatingCourseDto"> ДТО создаваемого курса. </param>
        //Task<int> CreateAsync(CreatingCourseDto creatingCourseDto);

        ///// <summary>
        ///// Изменить курс.
        ///// </summary>
        ///// <param name="id"> Иентификатор. </param>
        ///// <param name="updatingCourseDto"> ДТО редактируемого курса. </param>
        //Task UpdateAsync(int id, UpdatingCourseDto updatingCourseDto);

        ///// <summary>
        ///// Удалить курс.
        ///// </summary>
        ///// <param name="id"> Идентификатор. </param>
        //Task DeleteAsync(int id);
    }
}