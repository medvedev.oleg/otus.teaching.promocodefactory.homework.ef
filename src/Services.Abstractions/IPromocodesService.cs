using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Services.Contracts;

namespace Services.Abstractions
{
    /// <summary>
    /// Интерфейс сервиса работы с персоналом.
    /// </summary>
    public interface IPromocodesService
    {
        /// <summary>
        /// Получить все промокоды.
        /// </summary> 
        Task<List<PromoCodeDto>> GetAllAsync();

        /// <summary>
        /// Создать промокод.
        /// </summary>
        Task<PromoCode> CreateAsync(CreatingPromoCodeDto creatingPromoCodeDto);
 
        ///// <summary>
        ///// Изменить курс.
        ///// </summary>
        ///// <param name="id"> Иентификатор. </param>
        ///// <param name="updatingCourseDto"> ДТО редактируемого курса. </param>
        //Task UpdateAsync(int id, UpdatingCourseDto updatingCourseDto);

        ///// <summary>
        ///// Удалить курс.
        ///// </summary>
        ///// <param name="id"> Идентификатор. </param>
        //Task DeleteAsync(int id);
    }
}