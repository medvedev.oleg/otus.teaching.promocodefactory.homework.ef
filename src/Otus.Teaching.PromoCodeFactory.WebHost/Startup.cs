using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.EntityFramework;
using Otus.Teaching.PromoCodeFactory.Repositories;
using Services.Abstractions;
using Services.Implementations;
using Services.Implementations.Mapping;
using Services.Repositories.Abstractions;
using System.Text.Json.Serialization;


namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        private static IServiceCollection InstallAutomapper(IServiceCollection services)
        {
            services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));
            return services;
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {                
                cfg.AddProfile<Services.Implementations.Mapping.EmployeeMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.RoleMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.CustomerPreferenceProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.PromoCodeMappingsProfile>();
                cfg.AddProfile<Services.Implementations.Mapping.CreateCustomerMappingsProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            InstallAutomapper(services);

            services.AddControllers().AddJsonOptions(x => x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve);

            ///
            services.ConfigureContext(_configuration.GetConnectionString("DefaultConnection"));
            ///        

            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
            services.AddScoped<IEmployeeService, EmployeeService>();
            
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IRoleService, RoleService>();

            services.AddScoped<IPromocodesRepository, PromocodesRepository>();
            services.AddScoped<IPromocodesService, PromocodesService>();

            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<ICustomersService, CustomersService>();

            services.AddScoped<IPreferenceRepository, PreferencesRepository>();
            services.AddScoped<IPreferencesService, PreferencesService>();

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            });
        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}