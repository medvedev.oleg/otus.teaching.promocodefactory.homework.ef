using AutoMapper;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Services.Contracts; 

namespace WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности курса.
    /// </summary>
    public class EmployeeMappingsProfile : Profile
    {
        public EmployeeMappingsProfile()
        {
            CreateMap<EmployeeDto, EmployeeModel>();     
        }
    }
}
