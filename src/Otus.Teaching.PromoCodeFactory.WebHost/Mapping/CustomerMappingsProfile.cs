using AutoMapper;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Services.Contracts; 

namespace WebApi.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности курса.
    /// </summary>
    public class CustomerMappingsProfile : Profile
    {
        public CustomerMappingsProfile()
        {
            CreateMap<CustomerDto, CustomerModel>(); 
        }
    }
}
