﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Services.Abstractions;
using Services.Contracts;
using Services.Implementations;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomersService _service;

        public CustomersController(ICustomersService service)
        {
            _service = service;
        }

        [HttpGet]

        /// <summary>
        /// Получить данные всех пользователей
        /// </summary>
        /// <returns></returns>
        /// 
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var сustomers = await _service.GetAllAsync();

            var сustomersModelList = сustomers.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    CustomerPreferences = x.CustomerPreferences,
                }).ToList();

            return сustomersModelList;
        }

        /// <summary>
        /// Получить пользователя
        /// </summary>
        /// <returns></returns>
        ///
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomerAsync(Guid id)
        {
            var сustomer = await _service.GetAsync(id);

            if (сustomer == null) return NotFound();

            var сustomerModel = new CustomerShortResponse()
            {
                Id = сustomer.Id,
                Email = сustomer.Email,
                FirstName = сustomer.FirstName,
                LastName = сustomer.LastName,
                CustomerPreferences = сustomer.CustomerPreferences,
            };

            return сustomerModel;
        }

        /// <summary>
        /// Создать пользователя
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = await _service.CreateAsync(new CreatingCustomerDto()
            {
                Id = Guid.NewGuid(),
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
            });
            return Ok(customer);
        }

        /// <summary>
        /// Изменить пользователя
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            await _service.UpdateAsync(id, new UpdatingCustomerDto()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
            });

            return Ok();
        }

        /// <summary>
        /// Удалить пользователя
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            await _service.DeleteAsync(id);
            return Ok();
        }
    }
}