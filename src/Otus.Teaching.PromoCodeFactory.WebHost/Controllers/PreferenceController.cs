﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Services.Abstractions;

namespace Otus.Teaching.PreferenceController.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IPreferencesService _service;

        public PreferencesController(IPreferencesService preferencesService, IMapper mapper)
        {
            _service = preferencesService;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить все предпочтения
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PrefernceResponse>>> GetPreferencesAsync()
        {
            var preferences = await _service.GetAllAsync();

            var preferenceModelList = preferences.Select(x => new PrefernceResponse()
            {
                Id = x.Id,
                Name = x.Name,
            }).ToList();

            return preferenceModelList;
        }

    }
}