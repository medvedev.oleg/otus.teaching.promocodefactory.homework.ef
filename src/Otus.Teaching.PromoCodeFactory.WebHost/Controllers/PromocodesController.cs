﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Services.Abstractions;
using Services.Contracts;
using Services.Implementations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IPromocodesService _promocodesService;

        public PromocodesController(IPromocodesService promocodesService, IMapper mapper)
        {
            _promocodesService = promocodesService;
            _mapper  = mapper;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            //TODO: Получить все промокоды 
            var promocodes = await _promocodesService.GetAllAsync();

            var promocodesModelList = promocodes.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                ServiceInfo = x.ServiceInfo,
                BeginDate = x.BeginDate,
                EndDate = x.EndDate,
                PartnerName = x.PartnerName,
            }).ToList();

            return promocodesModelList;
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением          
            var promocode = await _promocodesService.CreateAsync(new CreatingPromoCodeDto() {
                PartnerName = request.PartnerName,
                Preference = request.Preference,
                PromoCode = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
            });
            return Ok(promocode);

        }
    }
}