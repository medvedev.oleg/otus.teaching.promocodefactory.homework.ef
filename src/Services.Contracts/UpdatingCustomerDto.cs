﻿namespace Services.Contracts
{
    public class UpdatingCustomerDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }
        public List<CustomerPreferenceDto> CustomerPreferences { get; set; }
        public List<PromoCodeDto> Promocodes { get; set; }
    }
}