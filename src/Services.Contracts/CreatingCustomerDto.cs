﻿namespace Services.Contracts
{
    public class CreatingCustomerDto         
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }
        public List<CustomerPreferenceDto> CustomerPreferences { get; set; }
        public List<PromoCodeDto> Promocodes { get; set; }
    }
}