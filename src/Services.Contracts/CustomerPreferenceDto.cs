﻿namespace Services.Contracts
{
    public class CustomerPreferenceDto        
    {
        public Guid Id { get; set; }

        public Guid CustomerId { get; set; }
        public CustomerDto Customer { get; set; }

        public Guid PreferenceId { get; set; }
        public PreferenceDto Preference { get; set; }
    }
}