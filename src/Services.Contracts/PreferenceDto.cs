﻿namespace Services.Contracts
{
    public class PreferenceDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}