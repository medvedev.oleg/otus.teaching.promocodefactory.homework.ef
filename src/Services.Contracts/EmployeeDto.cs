﻿namespace Services.Contracts
{
    public class EmployeeDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public RoleDto Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}
