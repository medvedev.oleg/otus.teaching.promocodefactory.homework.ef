﻿namespace Services.Contracts
{
    public class PromoCodeDto        
    {
        public Guid Id { get; set; }

        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }

        public string PartnerName { get; set; }

        public string PartnerManager { get; set; }
 
        public CustomerPreferenceDto CustomerPreferences  { get; set; }        
    }
}