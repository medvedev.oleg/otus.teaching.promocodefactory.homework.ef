﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с правами.
    /// </summary>
    public interface IRoleRepository : EfRepository<Role>
    {
 
    }
}