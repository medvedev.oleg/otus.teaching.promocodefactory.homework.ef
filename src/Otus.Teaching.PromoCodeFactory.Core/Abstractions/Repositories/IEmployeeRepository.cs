﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с сотрудниками.
    /// </summary>
    public interface IEmployeeRepository : EfRepository<Employee>
    {
 
    }
}