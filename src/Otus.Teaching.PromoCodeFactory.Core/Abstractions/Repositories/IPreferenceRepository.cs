﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с предпочтениями.
    /// </summary>
    public interface IPreferenceRepository : EfRepository<Preference>
    {
 
 
    }
}