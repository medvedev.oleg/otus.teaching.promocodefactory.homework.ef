﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Services.Repositories.Abstractions
{
    /// <summary>
    /// Репозиторий работы с промокодами.
    /// </summary>
    public interface IPromocodesRepository : EfRepository<PromoCode>
    {
 
    }
}