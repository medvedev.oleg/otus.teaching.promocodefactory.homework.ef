﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        //TODO: Списки Preferences и Promocodes  
        public List<CustomerPreference> CustomerPreferences { get; set; }
        public List<PromoCode> Promocodes { get; set; }
        public bool Deleted { get; set; }
    }
}