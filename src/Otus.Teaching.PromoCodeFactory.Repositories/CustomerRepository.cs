﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Services.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.EntityFramework;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Repositories
{
    /// <summary>
    /// Репозиторий работы с пользователями.
    /// </summary>
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(DatabaseContext context) : base(context)
        {
        }


        /// <summary>
        /// Запросить все сущности в базе.
        /// </summary>
        public async Task<List<Customer>> GetAllAsync()
        {
            return await Context.Set<Customer>().Include(x => x.CustomerPreferences).AsNoTracking().ToListAsync();
        }

    }
}
