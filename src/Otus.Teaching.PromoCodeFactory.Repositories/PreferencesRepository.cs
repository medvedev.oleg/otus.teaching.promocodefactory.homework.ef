﻿using Otus.Teaching.PromoCodeFactory.EntityFramework;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Services.Repositories.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.Repositories
{
    /// <summary>
    /// Репозиторий работы с пользователями.
    /// </summary>
    public class PreferencesRepository : Repository<Preference>, IPreferenceRepository
    {
        public PreferencesRepository(DatabaseContext context) : base(context)
        {
        }

    }
}
