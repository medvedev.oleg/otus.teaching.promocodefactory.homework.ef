﻿using Services.Repositories.Abstractions;
using Otus.Teaching.PromoCodeFactory.EntityFramework;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Repositories
{
    /// <summary>
    /// Репозиторий работы с промокодами.
    /// </summary>
    public class PromocodesRepository : Repository<PromoCode>, IPromocodesRepository
    {
        public PromocodesRepository(DatabaseContext context) : base(context)
        {             
        }

    }
}
