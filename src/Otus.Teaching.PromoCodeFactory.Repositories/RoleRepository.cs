﻿using Services.Repositories.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.EntityFramework;

namespace Otus.Teaching.PromoCodeFactory.Repositories
{
    /// <summary>
    /// Репозиторий работы с ролями.
    /// </summary>
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public RoleRepository(DatabaseContext context) : base(context)
        {             
        }

    }
}
