﻿using Services.Repositories.Abstractions;
using Services.Abstractions;
using AutoMapper;
using Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Services.Implementations
{
    /// <summary>
    /// Cервис работы с пользователями.
    /// </summary>
    public class CustomersService : ICustomersService
    {
        private readonly IMapper _mapper;
        private readonly ICustomerRepository _customerRepository;

        public CustomersService(
            IMapper mapper,
            ICustomerRepository customerRepository
            )
        {
            _mapper = mapper;
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить по id.
        /// </summary> 
        public async Task<CustomerDto> GetAsync(Guid id)
        {
            var customer = await _customerRepository.GetAsync(id);
            return _mapper.Map<Customer, CustomerDto>(customer);
        }

        /// <summary>
        /// Получить всех пользователей.
        /// </summary> 
        public async Task<List<CustomerDto>> GetAllAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            return _mapper.Map<List<Customer>, List<CustomerDto>>(customers);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="creatingCustomerDto"> ДТО создаваемого</param>
        /// <returns> Идентификатор. </returns>
        public async Task<Guid> CreateAsync(CreatingCustomerDto creatingCustomerDto)
        {
            var customer = _mapper.Map<CreatingCustomerDto, Customer>(creatingCustomerDto);
            var createdCustomer = await _customerRepository.AddAsync(customer);
            await _customerRepository.SaveChangesAsync();
            return createdCustomer.Id;
        }

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        /// <param name="updatingCustomerDto"> ДТО редактируемого </param>
        public async Task UpdateAsync(Guid id, UpdatingCustomerDto updatingCustomerDto)
        {
            var customer = await _customerRepository.GetAsync(id);
            if (customer == null)
            {
                throw new Exception($"Пользователь с идентфикатором {id} не найден");
            }

            customer.FirstName = updatingCustomerDto.FirstName;
            customer.LastName = updatingCustomerDto.LastName;
            customer.Email = updatingCustomerDto.Email;

            _customerRepository.Update(customer);
            await _customerRepository.SaveChangesAsync();
        }

        /// <summary>
        /// Удалить
        /// </summary>
        /// <param name="id"> Идентификатор. </param>
        public async Task DeleteAsync(Guid id)
        {
            var customer = await _customerRepository.GetAsync(id);
            customer.Deleted = true;
            await _customerRepository.SaveChangesAsync();
        }


    }
}