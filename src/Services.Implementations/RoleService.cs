﻿using Services.Repositories.Abstractions;
using Services.Abstractions;
using AutoMapper; 
using Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Services.Implementations
{
    /// <summary>
    /// Cервис работы с курсами.
    /// </summary>
    public class RoleService : IRoleService
    {
        private readonly IMapper _mapper;
        private readonly IRoleRepository _roleRepository;

        public RoleService(
            IMapper mapper,
            IRoleRepository roleRepository
            )
        {
            _mapper = mapper;
            _roleRepository = roleRepository;
        } 

        /// <summary>
        /// Получить все роли.
        /// </summary> 
        public async Task<List<RoleDto>> GetAllAsync()
        {
            var roles = await _roleRepository.GetAllAsync();
            return _mapper.Map<List<Role>, List<RoleDto>>(roles);
        } 
    }
}