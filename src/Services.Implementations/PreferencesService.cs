﻿using Services.Repositories.Abstractions;
using Services.Abstractions;
using AutoMapper;
using Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Services.Implementations
{
    /// <summary>
    /// Cервис работы с предпочтениями.
    /// </summary>
    public class PreferencesService : IPreferencesService
    {
        private readonly IMapper _mapper;
        private readonly IPreferenceRepository _preferencesRepository;

        public PreferencesService(
            IMapper mapper,
            IPreferenceRepository preferencesRepository
            )
        {
            _mapper = mapper;
            _preferencesRepository = preferencesRepository;
        }
         

        /// <summary>
        /// Получить все предпочтения.
        /// </summary> 
        public async Task<List<PreferenceDto>> GetAllAsync()
        {
            var preferences = await _preferencesRepository.GetAllAsync();
            return _mapper.Map<List<Preference>, List<PreferenceDto>>(preferences);
        }         
    }
}