using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Services.Contracts;

namespace Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера для сущности роли.
    /// </summary>
    public class RoleMappingsProfile : Profile
    {
        public RoleMappingsProfile()
        {                       
            CreateMap<Role, RoleDto>();
        }
    }
}
