using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Services.Contracts;

namespace Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера.
    /// </summary>
    public class CreateCustomerMappingsProfile : Profile
    {
        public CreateCustomerMappingsProfile()
        {
            CreateMap<Customer, CreatingCustomerDto>();
         }
    }
}
