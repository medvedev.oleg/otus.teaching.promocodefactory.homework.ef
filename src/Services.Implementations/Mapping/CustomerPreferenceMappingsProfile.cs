using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Services.Contracts;

namespace Services.Implementations.Mapping
{
    /// <summary>
    /// Профиль автомаппера.
    /// </summary>
    public class CustomerPreferenceProfile : Profile
    {
        public CustomerPreferenceProfile()
        {
            CreateMap<CustomerPreference, CustomerPreferenceDto>();
            CreateMap<Customer, CustomerDto>();
            CreateMap<Preference, PreferenceDto>();
        }
    }
}
