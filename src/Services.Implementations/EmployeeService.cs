﻿using Services.Repositories.Abstractions;
using Services.Abstractions;
using AutoMapper; 
using Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Services.Implementations
{
    /// <summary>
    /// Cервис работы с курсами.
    /// </summary>
    public class EmployeeService : IEmployeeService
    {
        private readonly IMapper _mapper;
        private readonly IEmployeeRepository _employeeRepository;
        //private readonly IBusControl _busControl;

        public EmployeeService(
            IMapper mapper,
            IEmployeeRepository employeeRepository
            //IBusControl busControl
            )
        {
            _mapper = mapper;
            _employeeRepository = employeeRepository;
           // _busControl = busControl;
        }

        /// <summary>
        /// Получить всех пользователя по id.
        /// </summary> 
        public async Task<EmployeeDto> GetAsync(Guid id)
        {
            var employee = await _employeeRepository.GetAsync(id);
               return _mapper.Map<Employee, EmployeeDto>(employee);
        }

        /// <summary>
        /// Получить всех пользователей.
        /// </summary> 
        public async Task<List<EmployeeDto>> GetAllAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();
            return _mapper.Map<List<Employee>, List<EmployeeDto>>(employees);
        }

        ///// <summary>
        ///// Получить курс.
        ///// </summary>
        ///// <param name="id"> Идентификатор. </param>
        ///// <returns> ДТО курса. </returns>
        //public async Task<CourseDto> GetByIdAsync(int id)
        //{
        //    var course = await _courseRepository.GetAsync(id);
        //    return _mapper.Map<Course, CourseDto>(course);
        //}

        ///// <summary>
        ///// Создать курс.
        ///// </summary>
        ///// <param name="creatingCourseDto"> ДТО создаваемого курса. </param>
        ///// <returns> Идентификатор. </returns>
        //public async Task<int> CreateAsync(CreatingCourseDto creatingCourseDto)
        //{
        //    var course = _mapper.Map<CreatingCourseDto, Course>(creatingCourseDto);
        //    var createdCourse = await _courseRepository.AddAsync(course);
        //    await _courseRepository.SaveChangesAsync();
        //    await _busControl.Publish(new MessageDto
        //    {
        //        Content = $"Course {createdCourse.Id} with name {createdCourse.Name} is added"
        //    });
        //    return createdCourse.Id;
        //}

        ///// <summary>
        ///// Изменить курс.
        ///// </summary>
        ///// <param name="id"> Идентификатор. </param>
        ///// <param name="updatingCourseDto"> ДТО редактируемого курса. </param>
        //public async Task UpdateAsync(int id, UpdatingCourseDto updatingCourseDto)
        //{
        //    var course = await _courseRepository.GetAsync(id);
        //    if (course == null)
        //    {
        //        throw new Exception($"Курс с идентфикатором {id} не найден");
        //    }

        //    course.Name = updatingCourseDto.Name;
        //    course.Price = updatingCourseDto.Price;
        //    _courseRepository.Update(course);
        //    await _courseRepository.SaveChangesAsync();
        //}

        ///// <summary>
        ///// Удалить курс.
        ///// </summary>
        ///// <param name="id"> Идентификатор. </param>
        //public async Task DeleteAsync(int id)
        //{
        //    var course = await _courseRepository.GetAsync(id);
        //    course.Deleted = true; 
        //    await _courseRepository.SaveChangesAsync();
        //}

    }
}