﻿using Services.Repositories.Abstractions;
using Services.Abstractions;
using AutoMapper;
using Services.Contracts;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Services.Implementations
{
    /// <summary>
    /// Cервис работы с курсами.
    /// </summary>
    public class PromocodesService : IPromocodesService
    {
        private readonly IMapper _mapper;
        protected readonly DatabaseContext _context;
        private readonly IPromocodesRepository _promocodesRepository;

        public PromocodesService(
            IMapper mapper,
            IPromocodesRepository promocodesRepository,
            DatabaseContext context
            )
        {
            _mapper = mapper;
            _context = context;
            _promocodesRepository = promocodesRepository;
        }

        /// <summary>
        /// Сохранить промокод.
        /// </summary> 
        public async Task<PromoCode> CreateAsync(CreatingPromoCodeDto creatingPromoCodeDto)
        {  
            var createdPromoCode = await _promocodesRepository.AddAsync(new PromoCode()
            {
                Id = Guid.NewGuid(),
                EndDate = DateTime.Now.ToString(),
                BeginDate = DateTime.Now.ToString(),
                PartnerManager = null,
                Code = creatingPromoCodeDto.PromoCode,
                Preference = _context.Preference.FirstOrDefault(x => x.Name == creatingPromoCodeDto.Preference),
                PartnerName = creatingPromoCodeDto.PartnerName,
                ServiceInfo = creatingPromoCodeDto.ServiceInfo
            });

            await _promocodesRepository.SaveChangesAsync();
            return createdPromoCode;
        }

        /// <summary>
        /// Получить все промокоды.
        /// </summary> 
        public async Task<List<PromoCodeDto>> GetAllAsync()
        {
            var promocodes = await _promocodesRepository.GetAllAsync();
            return _mapper.Map<List<PromoCode>, List<PromoCodeDto>>(promocodes);
        }

      
    }
}