﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Otus.Teaching.PromoCodeFactory.EntityFramework.Migrations
{
    /// <inheritdoc />
    public partial class test3 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPreference_Customer_CustomerId1",
                table: "CustomerPreference");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPreference_Preference_PreferenceId1",
                table: "CustomerPreference");

            migrationBuilder.DropIndex(
                name: "IX_CustomerPreference_CustomerId1",
                table: "CustomerPreference");

            migrationBuilder.DropIndex(
                name: "IX_CustomerPreference_PreferenceId1",
                table: "CustomerPreference");

            migrationBuilder.DropColumn(
                name: "CustomerId1",
                table: "CustomerPreference");

            migrationBuilder.DropColumn(
                name: "PreferenceId1",
                table: "CustomerPreference");

            migrationBuilder.AlterColumn<Guid>(
                name: "PreferenceId",
                table: "CustomerPreference",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<Guid>(
                name: "CustomerId",
                table: "CustomerPreference",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerPreference_CustomerId",
                table: "CustomerPreference",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerPreference_PreferenceId",
                table: "CustomerPreference",
                column: "PreferenceId");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPreference_Customer_CustomerId",
                table: "CustomerPreference",
                column: "CustomerId",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPreference_Preference_PreferenceId",
                table: "CustomerPreference",
                column: "PreferenceId",
                principalTable: "Preference",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPreference_Customer_CustomerId",
                table: "CustomerPreference");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPreference_Preference_PreferenceId",
                table: "CustomerPreference");

            migrationBuilder.DropIndex(
                name: "IX_CustomerPreference_CustomerId",
                table: "CustomerPreference");

            migrationBuilder.DropIndex(
                name: "IX_CustomerPreference_PreferenceId",
                table: "CustomerPreference");

            migrationBuilder.AlterColumn<int>(
                name: "PreferenceId",
                table: "CustomerPreference",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "TEXT");

            migrationBuilder.AlterColumn<int>(
                name: "CustomerId",
                table: "CustomerPreference",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "TEXT");

            migrationBuilder.AddColumn<Guid>(
                name: "CustomerId1",
                table: "CustomerPreference",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "PreferenceId1",
                table: "CustomerPreference",
                type: "TEXT",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CustomerPreference_CustomerId1",
                table: "CustomerPreference",
                column: "CustomerId1");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerPreference_PreferenceId1",
                table: "CustomerPreference",
                column: "PreferenceId1");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPreference_Customer_CustomerId1",
                table: "CustomerPreference",
                column: "CustomerId1",
                principalTable: "Customer",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPreference_Preference_PreferenceId1",
                table: "CustomerPreference",
                column: "PreferenceId1",
                principalTable: "Preference",
                principalColumn: "Id");
        }
    }
}
