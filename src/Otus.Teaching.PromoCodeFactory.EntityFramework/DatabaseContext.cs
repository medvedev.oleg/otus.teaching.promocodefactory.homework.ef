﻿using System;
using System.Data;
using System.Numerics;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.EntityFramework
{
    /// <summary>
    /// Контекст.
    /// </summary>
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        /// <summary>
        /// Сотрудники
        /// </summary>
        public DbSet<Employee> Employee { get; set; }

        /// <summary>
        /// Права
        /// </summary>
        public DbSet<Role> Role { get; set; }

        /// <summary>
        /// Пользователи
        /// </summary>
        public DbSet<Customer> Customer { get; set; }

        /// <summary>
        /// Предпочтения
        /// </summary>
        public DbSet<Preference> Preference { get; set; }

        /// <summary>
        /// Промежуточная таблица для связи Пользователей и Предпочтений
        /// </summary>
        public DbSet<CustomerPreference> CustomerPreference { get; set; }

        /// <summary>
        /// Промокоды
        /// </summary>
        public DbSet<PromoCode> PromoCode { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);

            modelBuilder.Entity<Employee>()
                .HasOne<Role>(e => e.Role);

             modelBuilder.Entity<CustomerPreference>()
               .HasOne<Customer>(cp => cp.Customer)
               .WithMany(c => c.CustomerPreferences)
               .HasForeignKey(c => c.CustomerId);

            modelBuilder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomerPreferences);


            modelBuilder.Entity<Employee>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Employee>().Property(c => c.LastName).HasMaxLength(100);

            modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(100);

            //Заполняем данными из FakeFactory
            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }
    }
}